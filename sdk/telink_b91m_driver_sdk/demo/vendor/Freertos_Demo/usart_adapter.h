#ifndef __USART_ADAPTER_H__
#define __USART_ADAPTER_H__

#include <stdint.h>
#include <stdlib.h>

uint8_t shell_uart_init(void);
uint8_t shell_uart_getchar(uint8_t * pdata);
void shell_uart_recivechar(char * buff,uint8_t len);

#endif
