/********************************************************************************************************
 * @file    main.c
 *
 * @brief   This is the source file for B91m
 *
 * @author  Driver Group
 * @date    2019
 *
 * @par     Copyright (c) 2019, Telink Semiconductor (Shanghai) Co., Ltd. ("TELINK")
 *          All rights reserved.
 *
 *          Licensed under the Apache License, Version 2.0 (the "License");
 *          you may not use this file except in compliance with the License.
 *          You may obtain a copy of the License at
 *
 *              http://www.apache.org/licenses/LICENSE-2.0
 *
 *          Unless required by applicable law or agreed to in writing, software
 *          distributed under the License is distributed on an "AS IS" BASIS,
 *          WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *          See the License for the specific language governing permissions and
 *          limitations under the License.
 *
 *******************************************************************************************************/
#include "app_config.h"
#include "calibration.h"
#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>
#include "littleshell.h"
#include "key.h"

extern void user_init();


uint16_t keyba_scan(void)
{
    uint16_t keystatus = 0x00;

    /* keyboard row0 init as output */
    gpio_function_en(KEY_BOARD_ROW1);
    gpio_output_en(KEY_BOARD_ROW1);
    gpio_set_level(KEY_BOARD_ROW1,1);

    /* keyboard row1 col1 col2 init as input */
    gpio_function_en(KEY_BOARD_ROW2);
    gpio_input_en(KEY_BOARD_ROW2);
    gpio_set_up_down_res(KEY_BOARD_ROW2,GPIO_PIN_PULLDOWN_100K);
    gpio_function_en(KEY_BOARD_COL1);
    gpio_input_en(KEY_BOARD_COL1);
    gpio_set_up_down_res(KEY_BOARD_COL1,GPIO_PIN_PULLDOWN_100K);
    gpio_function_en(KEY_BOARD_COL2);
    gpio_input_en(KEY_BOARD_COL2);
    gpio_set_up_down_res(KEY_BOARD_COL2,GPIO_PIN_PULLDOWN_100K);

    /* read row status */
    if(gpio_get_level(KEY_BOARD_COL1))
    {
        keystatus |= 0x01;
    }

    if(gpio_get_level(KEY_BOARD_COL2))
    {
        keystatus |= 0x02;
    }

    gpio_set_level(KEY_BOARD_ROW1,0);

    /* keyboard row0 init as output */
    gpio_function_en(KEY_BOARD_ROW2);
    gpio_output_en(KEY_BOARD_ROW2);
    gpio_set_level(KEY_BOARD_ROW2,1);

    /* keyboard row1 col1 col2 init as input */
    gpio_function_en(KEY_BOARD_ROW1);
    gpio_input_en(KEY_BOARD_ROW1);
    gpio_set_up_down_res(KEY_BOARD_ROW1,GPIO_PIN_PULLDOWN_100K);
    gpio_function_en(KEY_BOARD_COL1);
    gpio_input_en(KEY_BOARD_COL1);
    gpio_set_up_down_res(KEY_BOARD_COL1,GPIO_PIN_PULLDOWN_100K);
    gpio_function_en(KEY_BOARD_COL2);
    gpio_input_en(KEY_BOARD_COL2);
    gpio_set_up_down_res(KEY_BOARD_COL2,GPIO_PIN_PULLDOWN_100K);

    /* read row status */
    if(gpio_get_level(KEY_BOARD_COL1))
    {
        keystatus |= 0x04;
    }

    if(gpio_get_level(KEY_BOARD_COL2))
    {
        keystatus |= 0x08;
    }
    gpio_set_level(KEY_BOARD_ROW2,0);

    return keystatus;
}


static void led_task(void *pvParameters){
    uint8_t key_val=KEY_NONE,key_val1=KEY_NONE;
    char * key_str[] =  {
        "KEY_NONE",
        "KEY1_SHORT",
        "KEY1_LONG",
        "KEY1_CNTINUS",
        "KEY1_DOUBLE",
        "KEY2_SHORT",
        "KEY2_LONG",
        "KEY2_CNTINUS",
        "KEY2_DOUBLE",
        "KEY3_SHORT",
        "KEY3_LONG",
        "KEY3_CNTINUS",
        "KEY3_DOUBLE",
        "KEY4_SHORT",
        "KEY4_LONG",
        "KEY4_CNTINUS",
        "KEY4_DOUBLE",
        "KEY1_2_SHORT",
        "KEY1_2_LONG",
        "KEY1_2_CNTINUS",
        "KEY1_2_DOUBLE",
    };
    while(1){
        key_val = key_scan();
        if (key_val1 != key_val) {
            key_val1 = key_val;
            printf("KEY VALUE = %s.\r\n", key_str[key_val]);
        }
        vTaskDelay(8);
    }
    (void)(pvParameters);
}

static void led_task1(void *pvParameters){
    while(1){
        //printf("--\r\n");
        vTaskDelay(10000);
        //printf("&&\r\n");
        //printf("task2\r\n");
    }
    (void)(pvParameters);
}

unsigned int led(char argc,char ** argv)
{
    /*init gpio*/
    reg_gpio_pb_oen &= ~ GPIO_PB7;
    reg_gpio_pb_oen &= ~ GPIO_PB4;

    if(argc != 2)
        return 1;

    if(strcmp(argv[1],"on") == 0)
    {
        reg_gpio_pb_out |= GPIO_PB7;
        reg_gpio_pb_out |= GPIO_PB4;
    }
    else if(strcmp(argv[1],"off") == 0)
    {
        reg_gpio_pb_out &= ~GPIO_PB7;
        reg_gpio_pb_out &= ~GPIO_PB4;
    }

    return 0;
};
LTSH_FUNCTION_EXPORT(led,"test led on/off");

unsigned int time(char argc,char ** argv)
{
    printf("rtos time %d\r\n",xTaskGetTickCount()*2);
    printf("system timer %d\r\n",stimer_get_tick()/16000);
    return 0;
};
LTSH_FUNCTION_EXPORT(time,"test timer");


extern void vPortRestoreTask();

extern void littleshell_main_entry(void *pvParameters);
/**
 * @brief       This is main function
 * @return      none
 */
int main (void)
{
    sys_init(LDO_1P4_LDO_1P8, VBAT_MAX_VALUE_GREATER_THAN_3V6);
    //Note: This function can improve the performance of some modules, which is described in the function comments.
    //Called immediately after sys_init, set in other positions, some calibration values may not take effect.
    user_read_flash_value_calib();

    CCLK_24M_HCLK_24M_PCLK_24M;
    stimer_enable();
    user_init();

    if(0 ){                     // deepRetWakeUp
        vPortRestoreTask();
    }else{
        xTaskCreate( led_task, "tLed", configMINIMAL_STACK_SIZE*2, (void*)0, (tskIDLE_PRIORITY+1), 0 );
        xTaskCreate( led_task1, "tLed1", configMINIMAL_STACK_SIZE*2, (void*)0, (tskIDLE_PRIORITY+1), 0 );
        xTaskCreate( littleshell_main_entry, "shell", configMINIMAL_STACK_SIZE*4, (void*)0, (tskIDLE_PRIORITY+1), 0 );
        vTaskStartScheduler();
    }

    return 0;
}

extern void load_store_test(void);
extern void load_store_byte_test(void);
extern int sum_test(void);
extern int sum(int a,int b);
extern void test_print(void);


void printf_test(void)
{
    printf("TEST \r\n");
}

unsigned int asm_test(char argc,char ** argv)
{
    int cmd = 0;
    cmd = atoi(argv[1]);

    switch(cmd)
    {
    case 0:
        load_store_test();
        break;
    case 1:
        printf("load and stroe byte\r\n");
        load_store_byte_test();
        break;
    case 2:
        printf("load and stroe byte with u\r\n");
        load_store_byte_with_u_test();
        break;
    case 3:
        call_test();
        break;
    case 4:
        printf("add 0~100 = %d \r\n",sum_test());
        break;
    case 5:
        printf("sum %d\r\n",sum(10,300));
        break;
    case 6:
        test_print();
        break;
    default:
        break;
    }

    return 0;
};

LTSH_FUNCTION_EXPORT(asm_test,"test asm code");
