#include "ringbuffer.h"


static  RingBuffer uart_rx;

uint8_t shell_uart_init(void)
{

    /* init uart recive ring buffer */
    static uint8_t uart_rx_buff[128] = {0};
    RingBuffer_Init(&uart_rx,uart_rx_buff,128);

    return 0;
}


uint8_t shell_uart_getchar(uint8_t * pdata)
{
    return RingBuffer_Read(&uart_rx,pdata,1);
}

void shell_uart_recivechar(char * buff,uint8_t len)
{

    RingBuffer_Write(&uart_rx,(uint8_t *)buff,len);
}

