.data 
.align 3
test:
    .word 1000
hello:
    .string "hello"

.text
func:
la a0,test
li a1,100
sw a1,(a0)

li a0,1
la a1,hello
li a2,5
li a7,64
ecall
ret

.globl main #RARS 入口为main 函数需要使用.global 声明
main:
nop
nop
call func
la a0,test
li a1,100
sw a1,(a0)

li a0,1
la a1,hello
li a2,5
li a7,64
ecall
