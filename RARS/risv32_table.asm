.data 

.align 3
.global func_addr
func_addr:
    .word 0x80000
    .word 0x81000
    .word 0x82000

.align 3
.global func_string
func_string:
    .asciz "func_a"
    .asciz "func_b"
    .asciz "func_c"
    
.align 3
.global func_num
func_num:
    .word 3
    
.text
print_func_name:
    addi sp,sp,-32 # malloc 32byte sp buffer 
    sw ra,28(sp)   # save ra(return address) to sp+28
    mv t0,a0       # move a0 to t0
    la a1,func_string
    
.global main
main:
   li a0,0x80000
   call  print_func_name
    